import { async, fakeAsync, ComponentFixture, TestBed, tick } from '@angular/core/testing';
import { By } from '@angular/platform-browser';
import { DebugElement } from '@angular/core';
import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { RouterTestingModule } from '@angular/router/testing';
import { Observable } from 'rxjs/Rx';
import { Router, ActivatedRoute } from '@angular/router';
import { FormsModule } from '@angular/forms';

import { LoginComponent } from './login.component';

import { AuthenticationService } from '../shared/service/authentication-service';

describe('LoginComponent', () => {
    let fixture, comp;
    class AuthenticationServiceStub {
        public login(): Observable<any> {
            const user = {
                error: 'abc',
                name: 'tom'
            };
            return Observable.throw(user.error);
        }
        public logout() {
            sessionStorage.removeItem('currentUser');
        }
    }

    beforeEach(async(() => {
        TestBed.configureTestingModule({
            schemas: [CUSTOM_ELEMENTS_SCHEMA],
            declarations: [
                LoginComponent
            ],
            providers: [{ provide: AuthenticationService, useClass: AuthenticationServiceStub }],
            imports: [RouterTestingModule, FormsModule]
        }).compileComponents()
            .then(() => {
                fixture = TestBed.createComponent(LoginComponent);
                comp = fixture.componentInstance;
            });
    }));

    it(`should have default variables`, async(() => {
        expect(comp.model).toBeDefined();
        expect(comp.loading).toBeDefined();
        expect(comp.showError).toBeDefined();
        expect(comp.sessionId).toBeUndefined();
        expect(comp.error).toBeUndefined();
    }));

    it(`should logout on landing of login`, fakeAsync(() => {
        fixture.detectChanges();
        expect(sessionStorage.getItem('currentUser')).toBeNull();
    }));


    it(`should not login if invalid user`, async(() => {
        comp.model = {
            username: 'tomd',
            password: 'password'
        };
        comp.login();
        expect(comp.showError).toBeTruthy();
        expect(comp.loading).toBeFalsy();
    }));

});
