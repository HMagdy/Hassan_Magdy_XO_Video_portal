import {Component, OnInit} from '@angular/core';
import {Router, ActivatedRoute} from '@angular/router';

import {Md5} from 'ts-md5/dist/md5';

import {AuthenticationService} from '../shared/service/authentication-service';

@Component({
    moduleId: module.id,
    selector: 'app-video-login',
    templateUrl: 'login.component.html',
    styleUrls: ['./login.component.scss',
        '../../assets/css/bootstrap.min.css',
        '../../assets/css/dashboard.css',
        '../../assets/css/style.css'
    ],
})

export class LoginComponent implements OnInit {

    model: any = {};
    sessionId: string;
    loading = false;
    error;
    showError = false;

    constructor(
        private route: ActivatedRoute,
        private router: Router,
        private authenticationService: AuthenticationService) {}

    ngOnInit() {
        // reset login status
        this.authenticationService.logout();
    }
    /* when user logins, password will be encrypted and sent */
    login() {
        this.loading = true;
        const hash = Md5.hashStr(this.model.password);
        this.authenticationService.login(this.model.username, hash)
            .subscribe(data => {
                this.sessionId = data.sessionId;
                this.router.navigate(['/videos']);
            },
            error => {
                this.showError = true;
                this.error = error.error;
                this.loading = false;
            });
    }
}