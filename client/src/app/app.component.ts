import {AfterViewChecked, ChangeDetectorRef, Component, OnInit} from '@angular/core';
import {Router, ActivatedRoute} from '@angular/router';



@Component({
    selector: 'app-root',
    templateUrl: './app.component.html',
    styleUrls: [
        './app.component.css',
        '../assets/css/bootstrap.min.css',
        '../assets/css/dashboard.css',
        '../assets/css/style.css'
    ],
})

export class AppComponent implements OnInit, AfterViewChecked {
    title = 'XO Video Portal';
    user: string;
    constructor(
        private cdr: ChangeDetectorRef, 
        private router: Router,
        private route: ActivatedRoute) {}
    
    ngOnInit() {
        if (sessionStorage.getItem('currentUser')) {
            const currentUser = JSON.parse(sessionStorage.getItem('currentUser'));
            this.user = currentUser.username;
        }
    }
    
    home(){
        this.router.navigate(['/']);
    }
    
    videos(){
        this.router.navigate(['/videos']);
    }
    
    logout(){
        this.router.navigate(['/logout']);
    }
    
    login(){
        this.router.navigate(['/login']);

    }
    ngAfterViewChecked() {
        if (sessionStorage.getItem('currentUser')) {
            const currentUser = JSON.parse(sessionStorage.getItem('currentUser'));
            this.user = currentUser.username;
        } else {
            this.user = undefined;
        }
        this.cdr.detectChanges();
    }
}
