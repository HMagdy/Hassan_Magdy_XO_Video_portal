import { BrowserModule } from '@angular/platform-browser';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { FormsModule } from '@angular/forms';
import { HttpModule } from '@angular/http';
import { RouterModule, Routes } from '@angular/router';
import { AppComponent } from './app.component';
import { LoginComponent } from './login/login.component';
import { AppRoutingModule } from './app-routing.module';
import { VideosComponent } from './videos/videos.component';
import { VideoComponent } from './video/video.component';

import { AuthenticationService } from './shared/service/authentication-service';
import { BaseService } from './shared/service/base.service';
import { VideoService } from './shared/service/video.service';
import { CanActivateGuard } from './shared/guard/canActivateGuard';
import { StarRatingModule } from 'angular-star-rating';
import { HomeComponent } from './home/home.component';
import { NgModule, CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { CommonModule } from '@angular/common';


@NgModule({
  declarations: [
    AppComponent,
    LoginComponent,
    VideosComponent,
    VideoComponent,
    HomeComponent
  ],
  imports: [
    BrowserModule,
    FormsModule,
    HttpModule,
    RouterModule,
    BrowserAnimationsModule,
    AppRoutingModule,
    StarRatingModule.forRoot()
  ],
  providers: [AuthenticationService, BaseService, CanActivateGuard, VideoService],
  bootstrap: [AppComponent],
  schemas: [ CUSTOM_ELEMENTS_SCHEMA ]
})
export class AppModule { }
