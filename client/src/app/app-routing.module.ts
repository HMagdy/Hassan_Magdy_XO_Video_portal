import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { LoginComponent } from './login/login.component';
import { VideosComponent } from './videos/videos.component';
import { HomeComponent } from './home/home.component';
import { VideoComponent } from './video/video.component';
import { CanActivateGuard } from './shared/guard/canActivateGuard';

export const routes: Routes = [
  { path: '', redirectTo: '/home', pathMatch: 'full' },
  { path: 'login', component: LoginComponent },
  { path: 'logout', redirectTo: '/login', pathMatch: 'full' },
  { path: 'home', component: HomeComponent },
  { path: 'videos', component: VideosComponent, canActivate: [CanActivateGuard] },
  { path: 'video/:id', component: VideoComponent, canActivate: [CanActivateGuard] }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
