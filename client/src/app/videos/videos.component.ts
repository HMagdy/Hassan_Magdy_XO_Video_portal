import { Component, OnInit, Renderer, HostListener } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { Md5 } from 'ts-md5/dist/md5';

import { VideoService } from '../shared/service/video.service';
import { ServerUrl } from '../shared/config/serverurl';

@Component({
    moduleId: module.id,
    selector: 'app-video-videos',
    templateUrl: 'videos.component.html',
    styleUrls: ['videos.component.scss',
        '../../assets/css/bootstrap.min.css',
        '../../assets/css/dashboard.css',
        '../../assets/css/style.css']
})
export class VideosComponent implements OnInit {
    videos: Array<any> = [];
    currentPage = 0;
    end = false;
    buffer;
    url;

    constructor(
        private router: Router,
        private renderer: Renderer,
        private videoService: VideoService
    ) { }

    ngOnInit() {
        this.getVideos();
    }
    /* If user plays simultaneously videos, other videos will be paused */
    clicked(event) {
        const videos = document.getElementsByTagName('video');
        for (let i = 0; i < videos.length; i++) {
            if (videos[i] !== event.target) {
                videos[i].pause();
            }
        }
    }
    /* Ig user clicks on header, will redirect to details page */
    headerClick(event, videoName) {
        let id;
        this.videos.forEach((record) => {
            if (record.name === videoName) {
                id = record._id;
            }
        });
        this.url = '../video/' + id;
        this.router.navigate([this.url]);
    }

    loadBuffer() {
        const skip = 2 * this.currentPage;
        const limit = (skip > 10) ? 1 : 10;
        this.buffer = this.videoService.getVideos(skip, limit);
    }

    /* to add video and call buffer to load */
    addVideos(videos) {
        videos.forEach((element) => {
            const total = element.ratings.length;
            const sum = element.ratings.reduce((a, b) => {
                return a + b;
            });
            element['overAllRating'] = sum / total;
        });
        this.videos = this.videos.concat(videos);
        this.currentPage++;
        this.loadBuffer();
    }
    /* To get all videos */
    getVideos() {
        const skip = 0;
        const tempVideos = [];
        this.videoService.getVideos(skip).map(response => response.json())
            .subscribe((res) => {
                res.data.forEach((element, key) => {
                    tempVideos.push(element);
                    if (key + 1 === res.data.length) {
                        this.addVideos(tempVideos);
                    }
                });
            }, error => {
                alert(error);
            });
    }
    /* To rate video a post request will be sent with model */
    rateVideo(event, video) {
        let id;
        this.videos.forEach((record) => {
            if (record.name === video) {
                id = record._id;
            }
        });
        const model = {
            'videoId': id,
            'rating': event.rating
        };
        this.videoService.rateVideo(model).map(response => response.json())
            .subscribe((res) => {
                console.log(res);
            }, error => {
                alert(error);
            });
    }
}
