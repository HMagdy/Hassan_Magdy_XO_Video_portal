import { async, fakeAsync, ComponentFixture, TestBed, tick } from '@angular/core/testing';
import { By } from '@angular/platform-browser';
import { DebugElement } from '@angular/core';
import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { RouterTestingModule } from '@angular/router/testing';
import { Observable } from 'rxjs/Rx';
import { Router, ActivatedRoute } from '@angular/router';

import { VideosComponent } from './videos.component';
import { VideoService } from '../shared/service/video.service';
import { MockActivatedRoute } from '../shared/mocks/activate-route';

describe('VideosComponent', () => {
    let activeRoute: MockActivatedRoute;
    let fixture, comp;
    const dummyData = {
        json: function () {
            return {
                status: 'success',
                data: [{
                    description: 'React.js is a JavaScript ',
                    name: '[0] Getting Started With ReactJs',
                    ratings: [1, 5, 5, 4, 3],
                    url: 'videos/Getting_Started_With_React.js.mp4',
                    __v: 35,
                    _id: '5969cbea4f31d231b4d2a6eb'
                }]

            };
        }
    };
    class VideoServiceStub {
        public getVideos(): Observable<any> {
            return Observable.of(dummyData);
        }
        public rateVideo(event, id): Observable<any> {
            return Observable.of(dummyData);
        }
    }
    beforeEach(() => {
        activeRoute = new MockActivatedRoute();
    });
    beforeEach(async(() => {
        TestBed.configureTestingModule({
            schemas: [CUSTOM_ELEMENTS_SCHEMA],
            declarations: [
                VideosComponent
            ],
            providers: [{ provide: VideoService, useClass: VideoServiceStub }
            ],
            imports: [RouterTestingModule]
        }).compileComponents()
            .then(() => {
                fixture = TestBed.createComponent(VideosComponent);
                comp = fixture.componentInstance;
            });
    }));

    it(`should have a videos array defined`, async(() => {
        expect(comp.videos).toBeDefined();
        expect(comp.videos.length).toBe(0);
    }));

    it('should able to fetch videos', fakeAsync(() => {
        // ngOnInit will call getVideos
        fixture.detectChanges();
        expect(comp.videos.length).toBe(1);
    }));

    it('should able to rate video', fakeAsync(() => {
        const event = {
            rating: '4'
        };
        comp.rateVideo(event, '[0] Getting Started With ReactJs');
        fixture.detectChanges();
        expect(comp.videos.length).toBe(1);
    }));

    it('call addVideos method and it shoud update videos array', fakeAsync(() => {
        const data = [{
            description: 'React.js is a JavaScript ',
            name: '[0] Getting Started With ReactJs',
            ratings: [1, 5, 5, 4, 3],
            url: 'videos/Getting_Started_With_React.js.mp4',
            __v: 35,
            _id: '5969cbea4f31d231b4d2a6eb'
        }];
        comp.addVideos(data);
        expect(comp.videos.length).toBe(1);
    }));
});
