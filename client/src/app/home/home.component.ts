import {Component, OnInit, ViewEncapsulation} from '@angular/core';

@Component({
    selector: 'app-home',
    templateUrl: './home.component.html',
    styleUrls: ['./home.component.css', 
        '../../assets/css/bootstrap.min.css',
        '../../assets/css/dashboard.css',
        '../../assets/css/style.css'
    ],
    encapsulation: ViewEncapsulation.None
})

export class HomeComponent implements OnInit {
    title = 'XO Video Portal | Home';

    constructor() {}

    ngOnInit() {
    }

}
