import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';

import { Md5 } from 'ts-md5/dist/md5';

import { VideoService } from '../shared/service/video.service';
import { ServerUrl } from '../shared/config/serverurl';

@Component({
    moduleId: module.id,
    selector: 'app-video-video',
    templateUrl: 'video.component.html',
    styleUrls: ['video.component.scss',
        '../../assets/css/bootstrap.min.css',
        '../../assets/css/dashboard.css',
        '../../assets/css/style.css']
})
export class VideoComponent implements OnInit {
    video: Array<any> = [];

    constructor(
        private router: Router,
        private route: ActivatedRoute,
        private videoService: VideoService
    ) { }

    ngOnInit() {
        this.route.params.subscribe((param) => {
            if (param.id !== undefined) {
                this.getVideo(param.id);
            }
        });
    }
    /* To get a specific video  */
    getVideo(id) {
        const skip = 0;
        this.videoService.getVideo(id).map(response => response.json())
            .subscribe((res) => {
                console.log(res);
                res.data.forEach((element, key) => {
                    if (element._id === id) {
                        const total = element.ratings.length;
                        const sum = element.ratings.reduce((a, b) => {
                            return a + b;
                        });
                        element['overAllRating'] = sum / total;
                        this.video.push(element);
                    }
                });
            }, error => {
                alert(error);
            });
    }
    /*To rate a video and a post request will sent  */
    rateVideo(event, video) {
        let id;
        this.video.forEach((record) => {
            if (record.name === video) {
                id = record._id;
            }
        });
        const model = {
            'videoId': id,
            'rating': event.rating
        };
        this.videoService.rateVideo(model).map(response => response.json())
            .subscribe((res) => {
                console.log(res);
            },
            error => {
                alert(error);
            });
    }
    /* If user wants to browse another video */
    goBack() {
        this.router.navigate(['../../videos']);
    }
}
