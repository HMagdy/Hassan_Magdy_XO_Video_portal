import { async, fakeAsync, ComponentFixture, TestBed } from '@angular/core/testing';
import { By } from '@angular/platform-browser';
import { DebugElement } from '@angular/core';
import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { RouterTestingModule } from '@angular/router/testing';
import { Observable } from 'rxjs/Rx';

import { VideoComponent } from './video.component';
import { VideoService } from '../shared/service/video.service';

describe('VideoComponent', () => {
    let fixture, comp;
    const dummyData = {
        json: function () {
            return {
                status: 'success',
                data: [{
                    description: 'React.js is a JavaScript ',
                    name: '[0] Getting Started With ReactJs',
                    ratings: [1, 5, 5, 4, 3],
                    url: 'videos/Getting_Started_With_React.js.mp4',
                    __v: 35,
                    _id: '5969cbea4f31d231b4d2a6eb'
                }]

            };
        }
    };
    class VideoServiceStub {
        public getVideo(id): Observable<any> {
            return Observable.of(dummyData);
        }
        public rateVideo(event, id): Observable<any> {
            return Observable.of(dummyData);
        }
    }

    beforeEach(async(() => {
        TestBed.configureTestingModule({
            schemas: [CUSTOM_ELEMENTS_SCHEMA],
            declarations: [
                VideoComponent
            ],
            providers: [{ provide: VideoService, useClass: VideoServiceStub }],
            imports: [RouterTestingModule]
        }).compileComponents()
            .then(() => {
                fixture = TestBed.createComponent(VideoComponent);
                comp = fixture.componentInstance;
            });
    }));

    it(`should have a video variable`, async(() => {
        expect(comp.video).toBeDefined();
    }));

    it('should able to fetch video', fakeAsync(() => {
        comp.getVideo('5969cbea4f31d231b4d2a6eb');
        fixture.detectChanges();
        expect(comp.video.length).toBe(1);
    }));

    it('should able to rate video', fakeAsync(() => {
        const event = {
            rating: '4'
        };
        comp.rateVideo(event, '[0] Getting Started With ReactJs');
        fixture.detectChanges();
        expect(comp.video.length).toBe(0);
    }));
});
