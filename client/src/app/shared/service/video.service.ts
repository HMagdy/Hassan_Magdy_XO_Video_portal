import { Injectable } from '@angular/core';
import { Headers, Http, Request, RequestOptions, RequestOptionsArgs, Response, XHRBackend } from '@angular/http';
import { Observable } from 'rxjs/Observable';
import { ServerUrl } from '../config/serverurl';
import { BaseService } from '../service/base.service';

@Injectable()
export class VideoService extends BaseService {

    constructor(
        xHRBackend: XHRBackend,
        requestOptions: RequestOptions,
        private baseService: BaseService) {
        super(xHRBackend, requestOptions);
    }
    user = JSON.parse(sessionStorage.getItem('currentUser'));

    getVideos(skip?: number, limit?: number): Observable<any> {
        const limits = (limit) ? limit : null;
        if (this.user) {
            return this.baseService.apiCall(ServerUrl.getVideo + '?sessionId=' +
                this.user['sessionId'] + '&skip=' + skip + '&limit=' + limits, 'GET');
        }
    }

    getVideo(id): Observable<any> {
        if (this.user) {
            return this.baseService.apiCall(ServerUrl.getVideo + '?sessionId=' + this.user['sessionId'] + '&videoId=' + id, 'GET');
        }
    }

    rateVideo(model): Observable<any> {
        if (this.user) {
            return this.baseService.apiCall(ServerUrl.rateVideo + '?sessionId=' + this.user['sessionId'], 'POST', model);
        }
    }
}