import { Injectable } from '@angular/core';
import { Http, XHRBackend, RequestOptions, Request, RequestOptionsArgs, Response, Headers } from '@angular/http';
import { Observable } from 'rxjs/Observable';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/catch';

@Injectable()
export class BaseService extends Http {

    constructor(backend: XHRBackend, options: RequestOptions) {
        const token = sessionStorage.getItem('currentUser'); // your custom token getter function here
        options.headers.set('Authorization', `Bearer ${token}`);
        super(backend, options);
    }

    apiCall(url: string, verb: string, payload?: any, contentType?: string): Observable<any> {
        const headers = new Headers();
        headers.append('Content-Type', (contentType || 'application/json'));
        const requestOptions = new Request({
            method: verb,
            url: url,
            headers: headers
        });
        if (verb === 'POST') {
            return this.post(url, payload);
        }
        return this.request(requestOptions);
    }

    request(url: string | Request, options?: RequestOptionsArgs): Observable<Response> {
        const token = sessionStorage.getItem('currentUser');
        if (typeof url === 'string') {
            if (!options) {
                // let's make option object
                options = { headers: new Headers() };
            }
            options.headers.set('Authorization', `Bearer ${token}`);
        } else {
            // we have to add the token to the url object
            url.headers.set('Authorization', `Bearer ${token}`);
        }
        return super.request(url, options).catch(this.catchAuthError(this));
    }

    post(url: string, body: any): Observable<Response> {
        return super.post(url, body).catch(this.catchAuthError(this));
    }

    private catchAuthError(self: BaseService) {
        // we have to pass BaseService's own instance here as `self`
        return (res: Response) => {
            console.log(res);
            if (res.status === 401 || res.status === 403) {
                // if not authenticated
                console.log(res);
            }
            return Observable.throw(res);
        };
    }
}