import { Injectable } from '@angular/core';
import { Headers, Http, Response } from '@angular/http';
import { Observable } from 'rxjs/Observable';
import 'rxjs/add/observable/throw';
import { ServerUrl } from '../config/serverurl';

@Injectable()
export class AuthenticationService {

    constructor(private http: Http) { }

    login(username: string, password: any) {
        return this.http.post(ServerUrl.loginurl, { username: username, password: password })
            .map((response: Response) => {
                const user = response.json();
                if (user && user.sessionId) {
                    // store user details and sessionId in session storage to keep user logged in between page refreshes
                    sessionStorage.setItem('currentUser', JSON.stringify(user));
                }

                if (user && user.error) {
                    throw Observable.throw(user.error);
                }

                return user;
            });
    }

    logout() {
        // remove user from session storage to log user out
        sessionStorage.removeItem('currentUser');
    }
}