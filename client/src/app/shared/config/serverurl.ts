const serverurl = 'http://localhost:3000';

/* necessary url can be accessed through const Serverurl */
export const ServerUrl = {
    loginurl: serverurl + '/user/auth',
    logouturl: serverurl + '/user/logout',
    getVideo: serverurl + '/videos',
    rateVideo: serverurl + '/video/ratings'
};
