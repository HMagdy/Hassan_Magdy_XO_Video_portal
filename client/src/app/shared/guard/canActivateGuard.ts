import { Injectable } from '@angular/core';
import { Router, ActivatedRoute, CanActivate, ActivatedRouteSnapshot, RouterStateSnapshot } from '@angular/router';

@Injectable()
export class CanActivateGuard implements CanActivate {

    constructor(private router: Router) { }
    /* CanActivate guard is used whether to activate partcular route user is accessing */
    public canActivate(route: ActivatedRouteSnapshot, state: RouterStateSnapshot) {
        const currentUser = JSON.parse(sessionStorage.getItem('currentUser'));
        if (currentUser === undefined || currentUser === null) {
            this.router.navigate(['/login']);
            return false;
        }
        return true;
    }
}
