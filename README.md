# XO Video Portal backend by Hassan Magdy Saad ;)
This is the backend API code that needs to be consumed by front-end applications.


0. make sure you have:

- npm
- ng
- mongodb
    
```
    sudo service mongod status
```

1- tab one:
cd /var/www/html/MEAN/Hassan_Magdy_Video_portal
npm install
npm start
you can open 

http://localhost:3000/


2- tab two:
cd /var/www/html/MEAN/Hassan_Magdy_Video_portal/client
npm install
ng build

ng test


npm start
or
ng serve --open

http://localhost:4200/home